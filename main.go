package main

import (
	"context"
	"fmt"
	"net/http"

	"github.com/go-redis/redis/v8"
)

var (
	rdClient *redis.Client
)

func main() {
	fmt.Println("Start sse server")

	rdClient = redis.NewClient(&redis.Options{
		Addr:     "localhost:6379",
		Password: "",
		DB:       0,
	})
	defer rdClient.Close()

	broker := &Broker{
		Connects:   make(chan *Connect),
		Disconnect: make(chan *Connect),
	}
	server := &http.Server{
		Addr:    ":3333",
		Handler: broker,
	}

	go broker.Listen()

	server.ListenAndServe()
}

type Connect struct {
	Key            string
	MessageChannel chan []byte
	Ctx            context.Context
	CancelFunc     context.CancelFunc
}

type Broker struct {
	Connects   chan *Connect
	Disconnect chan *Connect
}

func (this *Broker) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	flusher, ok := w.(http.Flusher)

	if !ok {
		fmt.Errorf("Not supported flushing")
	}

	w.Header().Set("Content-type", "text/event-stream")
	w.Header().Set("Cache-Control", "no-cache")
	w.Header().Set("Connection", "keep-alive")
	w.Header().Set("Access-Control-Allow-Origin", "*")

	messageChannel := make(chan []byte)
	ctx, cancel := context.WithCancel(context.Background())
	connect := &Connect{
		Key:            "1",
		MessageChannel: messageChannel,
		Ctx:            ctx,
		CancelFunc:     cancel,
	}

	defer func() {
		this.Disconnect <- connect
	}()

	go func() {
		<-r.Context().Done()
		fmt.Println("connect closed")
		this.Disconnect <- connect
	}()

	this.Connects <- connect

	for {
		msg := <-messageChannel
		_, err := fmt.Fprintf(w, "%s\n", string(msg))
		if err != nil {
			fmt.Printf("error: ", err)
		}
		flusher.Flush()
	}
}

func (this *Broker) Listen() {
	for {
		select {
		case connect := <-this.Connects:
			{
				topic := "sub-key-" + connect.Key
				pubSub := rdClient.Subscribe(connect.Ctx, topic)
				go func() {
					for {
						select {
						case msg := <-pubSub.Channel():
							{
								connect.MessageChannel <- []byte(msg.Payload)
								fmt.Println("Resieve msg: " + msg.Payload)
							}
						case <-connect.Ctx.Done():
							{
								err := pubSub.Unsubscribe(connect.Ctx, topic)
								if err != nil {
									fmt.Println("Failed to unsubscribe from channel")
								}
								pubSub = nil
								connect = nil
								fmt.Println("Client unsubscribed")
								return
							}
						}
					}
				}()

				fmt.Println("New client connected key " + topic)
			}

		case disconnect := <-this.Disconnect:
			{
				disconnect.CancelFunc()
				fmt.Println("Disconnected")
			}
		}
	}
}
